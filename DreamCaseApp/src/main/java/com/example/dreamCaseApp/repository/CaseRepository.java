package com.example.dreamCaseApp.repository;

import com.example.dreamCaseApp.entity.Case;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaseRepository extends JpaRepository<Case, Long> {
    /* the reposotry of SPRNG DATA JPA */
}
