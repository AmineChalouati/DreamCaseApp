package com.example.dreamCaseApp.service;

import com.example.dreamCaseApp.entity.Case;

import java.util.List;

public interface CaseService {
    public List<Case> findAll();
    public Case readOneCase(Long idCase);

    public Case saveCase(Case theCase);
    public void deleteCase(Long idCase);
}
