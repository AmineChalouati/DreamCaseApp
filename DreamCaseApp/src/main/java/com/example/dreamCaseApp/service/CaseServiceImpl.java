package com.example.dreamCaseApp.service;

import com.example.dreamCaseApp.entity.Case;
import com.example.dreamCaseApp.repository.CaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// enable service
@Service
public class CaseServiceImpl implements CaseService {
    private CaseRepository caseRepository;

    @Autowired
    public CaseServiceImpl(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    @Override
    public List<Case> findAll() {
        return caseRepository.findAll();
    }

    @Override
    public Case saveCase(Case theCase) {
        return caseRepository.save(theCase);
    }

    // working with Optional (introduced in java8)
    @Override
    public Case readOneCase(Long idCase) {
        Optional<Case> result = caseRepository.
                findById(idCase);
        Case theCase = null;
        if (result.isPresent()) {
            theCase = result.get();
        } else {
            // if we didnt find the case
            throw new RuntimeException("Can't find the case n°- case id : " + idCase);

        }
        return theCase;
    }

    @Override
    public void deleteCase(Long idCase) {
        caseRepository.deleteById(idCase);
    }
}
