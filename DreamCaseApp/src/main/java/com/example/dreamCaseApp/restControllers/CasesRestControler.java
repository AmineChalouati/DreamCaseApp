package com.example.dreamCaseApp.restControllers;


import com.example.dreamCaseApp.entity.Case;
import com.example.dreamCaseApp.service.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cases")
public class CasesRestControler {
    private CaseService caseService;

    @Autowired
    public CasesRestControler(CaseService theCaseService) {
        caseService = theCaseService;
    }

    //  Add mapping for Create (POST method)
    @PostMapping
    public Case createCase(@RequestBody Case theCase) {
        theCase.setCaseId(null);
        caseService.saveCase(theCase);
        return theCase;
    }


    // Add mapping to  test all the cases
    @GetMapping
    public List<Case> findAll() {
        return caseService.findAll();
    }

    //  Add mapping to Read (GET method)
    @GetMapping("/{caseId}")
    public Case readOneCase(@PathVariable Long caseId) {
        Case theCase = caseService.readOneCase(caseId);
        if (theCase == null) {
            throw new RuntimeException(
                    "the caes id " + caseId + "is not found"
            );
        }
        return theCase;
    }

    // (PUT method)
    @PutMapping("/{caseId}")
    public Case updateCase(@RequestBody Case theCase, @PathVariable Long caseId) {
        theCase.setCaseId(caseId);
        theCase.setCreationDate(caseService.readOneCase(caseId).getCreationDate());
        return caseService.saveCase(theCase);
    }

    // (DELETE method).
    @DeleteMapping("/{caseId}")
    public String deleteCase(@PathVariable Long caseId) {
        Case theCase = caseService.readOneCase(caseId);
        if (theCase == null) {
            throw new RuntimeException(
                    "The case id is not found " + caseId
            );
        }
        caseService.deleteCase(caseId);
        return "The below case was Deleted succefully, the case id: " + caseId;
    }
}
